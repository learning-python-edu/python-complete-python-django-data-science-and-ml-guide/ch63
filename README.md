# Chapter 63: Virtual Environments

## Create new virtual environment

To create new Python virtual environment execute following command,

on Windows:
```shell
python -m venv venv
```

On Linux/MacOS:
```shell
python3 -m venv venv
```

Last argument is a name of virtual environment and it can be name whatever you like but convention is to call it `venv`.

## Activate virtual environment

Virtual environment can be activated with the following command:

On Windows:
```shell
source venv/Scripts/activate
```
On Linux/MacOS:
```shell
source venv/bin/activate
```

To check that virtual environment is activated, execute following command:
```shell
which python
```

The output should point to the Python locate in `venv/Scripts` folder on Windows or to `venv/bin` directory on Linux/MacOS.

## Deactivate virtual environment

To deactivate Python virtual environment, execute following command:
```shell
deactivate
```

## List locally installed package

To check packages available for the activated virtual environment, execute following command:
```shell
pip list
```

## Install package for activated virtual environment

To install Python packages for virtual environment, execute following command in the directory where virtual 
environment is activated:
```shell
pip install requests pandas
```
(In this example `requests`, `pandas` packages and its dependencies will be installed)

To check installed packages, execute following command:
```shell
pip freeze
```

## Save project dependencies into file

It is possible to save current project's dependencies into `requirements.txt` file, wich then can be used by other
developers for getting project dependencies on their machines.

To do this execute following command:
```shell
pip freeze > requirements.txt
```

`requirements.txt` file is a convention used by Python community for Python projects' dependencies distribution.

## Install project dependencies form requirements.txt

To install Python project's dependencies from `requirements.txt`, execute following command:
```shell
pip install -r requirements.txt
```
